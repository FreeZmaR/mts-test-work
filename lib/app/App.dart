import 'package:flutter/material.dart';
import 'package:mts_test/app/Screens/Home/HomeScreen.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) => MaterialApp(
        home: HomeScreen(title: 'Mts Test Solution'),
      );
}
