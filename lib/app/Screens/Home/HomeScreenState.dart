import 'dart:async';
import 'dart:io';
import 'package:mts_test/app/Classes/ImageHelper.dart';
import 'package:mts_test/app/Widgets/Element/Button/IndicateTextButton/IndicateTextButton.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mts_test/app/Screens/Home/HomeScreen.dart';
import 'package:mts_test/app/Screens/Home/HomeScreenLayout.dart';
import 'package:mts_test/app/Widgets/Element/AppBar/AppBarDefault.dart';

class HomeScreenState extends State<HomeScreen> {
  bool _inProgress = false;
  String _progressMessage = _messages["empty"];
  Widget _imageWidget;
  final int _imageSize = 1200;
  static Map<String, String> _messages = {
    "in_progress": "Image in the process",
    "empty" : "No Image"
  };

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBarDefault(
        title: widget.title,
        leading: Container(
          transform: Matrix4.translationValues(20, 0, 0),
          child: GestureDetector(
            onTap: loadAssetImage,
            child: IndicateTextButton(
              text: "Assets",
              activeStatus: _inProgress,
            ),
          ),
        ),
      ),
      body: HomeScreenLayout(
        description: widget.description,
        onButtonPressed: this.onPickImage,
        progressStatus: _inProgress,
        image: _imageWidget ?? Text(_progressMessage),
      ),
    );
  }

  void onPickImage() {
    Future<File> file = ImagePicker.pickImage(source: ImageSource.gallery);
    setStatusInProgress();
    loadFileImage(file);
  }

  void setStatusInProgress() {
    setState(() {
      _imageWidget = null;
      _inProgress = true;
      _progressMessage = _messages["in_progress"];
    });
  }

  void loadFileImage(Future<File> file) async {
    File image = await file;

    if (image == null) {
      setState(() {
        _inProgress = false;
        _progressMessage = _messages["empty"];
      });
      return;
    }

    ImageHelper imageHelper =
        await ImageHelper.fromFile(image).resize(size: _imageSize);

    Widget imageWidget = await imageHelper.cut(height: 180, width: 320);

    setState(() {
      _inProgress = false;
      _imageWidget = imageWidget;
    });
  }

  void loadAssetImage() async {
    if (_inProgress) {
      return;
    }

    setStatusInProgress();

    ImageHelper imageHelper =
        await ImageHelper.fromAssets(widget.assetImagePath)
            .resize(size: _imageSize);

    Widget image = await imageHelper.cut(height: 180, width: 320);

    setState(() {
      _inProgress = false;
      _imageWidget = image;
    });
  }
}
