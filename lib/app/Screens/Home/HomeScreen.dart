import 'package:flutter/material.dart';
import 'package:mts_test/app/Screens/Home/HomeScreenState.dart';

class HomeScreen extends StatefulWidget {
  final String title;
  final String description = "Выберите изображение для сжатия до 1200х1200,"
      " после будет вырезана часть из центра изображения";
  final String assetImagePath = "assets/images/sea.jpg";

  HomeScreen({Key key, this.title}) : super(key: key);

  @override
  HomeScreenState createState() => HomeScreenState();
}
