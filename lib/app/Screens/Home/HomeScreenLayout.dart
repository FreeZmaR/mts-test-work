import 'package:flutter/material.dart';
import 'package:mts_test/app/Widgets/Element/Button/ProgressButton/ProgressButton.dart';
import 'package:mts_test/app/Widgets/Layout/MainLayout.dart';

class HomeScreenLayout extends StatelessWidget {
  final String description;
  final Function onButtonPressed;
  final bool progressStatus;
  final Widget image;

  HomeScreenLayout({
    Key key,
    this.description,
    this.onButtonPressed,
    this.progressStatus,
    this.image,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MainLayout(
      headerLayout: Container(
        alignment: FractionalOffset.center,
        height: 300,
        padding: EdgeInsets.only(left: 20, right: 20),
        child: image ?? Container(),
      ),
      contentLayout: Container(
        alignment: FractionalOffset.center,
        height: 60.0,
        child: ProgressButton(
          onPressed: onButtonPressed,
          buttonText: "Выбрать файл",
          progressStatus: progressStatus,
        ),
      ),
      footerLayout: Container(
        alignment: FractionalOffset.center,
        padding: EdgeInsets.only(left: 20, right: 20),
        child: Text(
          description,
          style: TextStyle(fontWeight: FontWeight.w500, fontSize: 15),
        ),
      ),
    );
  }
}
