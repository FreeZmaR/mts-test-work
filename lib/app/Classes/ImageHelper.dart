import 'dart:typed_data';
import 'dart:ui' as Ui;
import 'dart:io' as Io;
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ImageHelper {
  Future<ImageInfo> _imageInfo;
  ImageProvider _provider;
  String _imagePath;
  Uint8List _imageBytes;

  ImageHelper.fromAssets(this._imagePath);

  ImageHelper.fromFile(Io.File imageFile)
      : this._imageBytes = imageFile.readAsBytesSync().buffer.asUint8List();

  ///загружаем информацию о картинки из потока
  _initImageInfo() async {
    Completer<ImageInfo> completer = Completer<ImageInfo>();
    Uint8List imageBytes = _imageBytes != null
        ? _imageBytes
        : await rootBundle
            .load(_imagePath)
            .then((bytes) => bytes.buffer.asUint8List());

    this._provider = Image.memory(imageBytes).image;

    this
        ._provider
        .resolve(ImageConfiguration())
        .addListener(ImageStreamListener((ImageInfo imageInfo, bool _) {
      completer.complete(imageInfo);
    }));

    this._imageInfo = completer.future;
  }

  /// Изменения размера картинки
  /// [size], [height], [width] - double or int
  /// return - self;
  /// Если передан [size] то выбирается большая из сторон и ей присваивается [size]
  /// иначе присваивает переданные [width], [height] или возвращаем - self
  /// Изменяем размер картинки после перезаписываем информацию о ней
  Future<ImageHelper> resize({num size, num height, num width}) async {
    if (this._imageInfo == null) {
      await this._initImageInfo();
    }

    ImageInfo imageInfo = await this._imageInfo;
    final image = imageInfo.image;

    this._imageInfo = null;

    ResizeImage resizeImage;
    //TODO: fix вложенность
    if (size != null) {
      if (image.height > image.width) {
        resizeImage = ResizeImage(this._provider, height: size);
      } else {
        resizeImage = ResizeImage(this._provider, width: size);
      }
    } else {
      if (height == null && width == null) {
        return this;
      }

      resizeImage = ResizeImage(this._provider, width: width, height: height);
    }

    Completer<ImageInfo> completer = Completer<ImageInfo>();
    resizeImage
        .resolve(ImageConfiguration())
        .addListener(ImageStreamListener((ImageInfo resizeImageInfo, bool _) {
      completer.complete(resizeImageInfo);
    }));

    this._imageInfo = completer.future;

    return this;
  }

  ///Вырезает прямоугольник переданных размеров ([height], [width]) из картинки
  /// return - Widget
  Future<Widget> cut({double height, double width}) async {
    if (this._imageInfo == null) {
      await this._initImageInfo();
    }

    ImageInfo image = await this._imageInfo;

    Ui.PictureRecorder recorder = Ui.PictureRecorder();
    Ui.Canvas canvas = Ui.Canvas(recorder);

    Offset offset = Offset((image.image.width) / 2, (image.image.height) / 2);

    Rect src = Rect.fromCenter(center: offset, width: width, height: height);
    Rect dst = Rect.fromLTWH(0, 0, width, height);
    canvas.drawImageRect(image.image, src, dst, Paint());
    Ui.Image result =
        await recorder.endRecording().toImage(width.toInt(), height.toInt());

    return RawImage(image: result);
  }

  ///Возвращаем картинку с текущей информацией
  ///return  - Widget
  Future<Widget> image() async {
    if (this._imageInfo == null) {
      await this._initImageInfo();
    }
    ImageInfo image = await this._imageInfo;

    return RawImage(image: image.image, scale: image.scale);
  }
}
