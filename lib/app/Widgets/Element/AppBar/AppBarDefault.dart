import 'package:flutter/material.dart';

class AppBarDefault extends AppBar {
  @override
  final Widget title;

  @override
  final Color backgroundColor = Color.fromRGBO(227, 6, 15, 1);

  @override
  final Widget leading;

  AppBarDefault({Key key, String title, this.leading})
      : title = getTitle(title),
        super(key: key);

  static Widget getTitle(String title) => Center(
        child: Text(title, style: TextStyle(color: Colors.white)),
      );
}
