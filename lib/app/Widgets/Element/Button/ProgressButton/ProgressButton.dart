import 'package:flutter/material.dart';
import 'ProgressButtonState.dart';

class ProgressButton extends StatefulWidget {
  final String buttonText;
  final bool progressStatus;
  final Function onPressed;

  ProgressButton({
    Key key,
    this.buttonText,
    this.onPressed,
    this.progressStatus = false,
  }) : super(key: key);

  @override
  ProgressButtonState createState() => ProgressButtonState();
}
