import 'package:flutter/material.dart';
import 'package:mts_test/app/Widgets/Element/Button/ProgressButton/ProgressButton.dart';
import 'package:mts_test/app/Widgets/Style/Icon/IconCircleDone.dart';

class ProgressButtonState extends State<ProgressButton> {
  bool _showCompleteIcon = false;

  @override
  Widget build(BuildContext context) {
    return renderButton();
  }

  void hideCompleteIcon() {
    Future.delayed(
        Duration(milliseconds: 300),
        () => setState(() {
              _showCompleteIcon = false;
            }));
  }

  Widget renderButton() {
    if (_showCompleteIcon && !widget.progressStatus) {
      hideCompleteIcon();
      return IconCircleDone();
    }

    if (widget.progressStatus) {
      setState(() {
        _showCompleteIcon = true;
      });
      return CircularProgressIndicator();
    }

    return RaisedButton(
      onPressed: () => {
        setState(() {
          widget.onPressed();
        })
      },
      padding: EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
      colorBrightness: Brightness.dark,
      color: Colors.blue,
      child: Text(widget.buttonText),
    );
  }
}
