import 'package:flutter/material.dart';

class IndicateTextButton extends StatelessWidget {
  final String text;
  final bool activeStatus;

  IndicateTextButton({Key key, this.text, this.activeStatus}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        color: getBackgroundColor(),
      ),
      padding: EdgeInsets.only(
        top: 16,
        bottom: 15,
      ),
      child: Text(
        text,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 16,
          fontStyle: FontStyle.italic,
          fontWeight: FontWeight.w600,
        ),
      ),
    );
  }

  Color getBackgroundColor() =>
      activeStatus ? Colors.orangeAccent : Colors.blue;
}
