import 'package:flutter/material.dart';

class MainLayout extends StatelessWidget {
  final Widget headerLayout;
  final Widget contentLayout;
  final Widget footerLayout;

  MainLayout({this.headerLayout, this.contentLayout, this.footerLayout});

  @override
  Widget build(BuildContext context) => Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          this.headerLayout ?? Container(),
          this.contentLayout ?? Container(),
          this.footerLayout ?? Container(),
        ],
      );
}
