import 'package:flutter/material.dart';

class IconCircleDone extends StatelessWidget {
  final double size;

  IconCircleDone({Key key, this.size = 24}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20.0),
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: Colors.green,
      ),
      child: Icon(Icons.done, color: Colors.white, size: size),
    );
  }
}
